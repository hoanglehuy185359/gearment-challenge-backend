package common

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type CodeResponse int

const (
	ErrorCodeBadRequest   CodeResponse = http.StatusBadRequest
	ErrorCodeUnauthorized CodeResponse = http.StatusUnauthorized
	ErrorCodeNotFound     CodeResponse = http.StatusNotFound
	ErrorCodeSystemError  CodeResponse = http.StatusInternalServerError
	ErrorCodeForbidden    CodeResponse = http.StatusForbidden
)

type Error struct {
	Code       CodeResponse `json:"code"`
	Message    string       `json:"message"`
	TraceID    string       `json:"trace_id,omitempty"`
	Detail     string       `json:"detail"`
	HTTPStatus int          `json:"http_status"`
}

func (e *Error) Error() string {
	return fmt.Sprintf("code:[%d], message:[%s], detail:[%s]", e.Code, e.Message, e.Detail)
}

func (e *Error) GetHttpStatus() int {
	return e.HTTPStatus
}

func (e *Error) GetCode() CodeResponse {
	return e.Code
}

func (e *Error) GetMessage() string {
	return e.Message
}

func (e *Error) SetTraceId(traceId string) *Error {
	e.TraceID = fmt.Sprintf("%s:%d", traceId, time.Now().Unix())
	return e
}

func (e *Error) SetHTTPStatus(status int) *Error {
	e.HTTPStatus = status
	return e
}

func (e *Error) SetMessage(msg string) *Error {
	e.Message = msg
	return e
}

func (e *Error) SetDetail(detail string) *Error {
	e.Detail = detail
	return e
}

func (e *Error) GetDetail() string {
	return e.Detail
}

func (e *Error) ToJSon() string {
	data, err := json.Marshal(e)
	if err != nil {
		return "marshal error failed"
	}
	return string(data)
}

var (
	// Status 4xx ********

	ErrUnauthorized = func(ctx context.Context) *Error {
		traceId := GetTraceId(ctx)
		return &Error{
			Code:       ErrorCodeUnauthorized,
			Message:    DefaultUnauthorizedMessage,
			TraceID:    traceId,
			HTTPStatus: http.StatusUnauthorized,
		}
	}

	ErrNotFound = func(ctx context.Context, object, status string) *Error {
		traceId := GetTraceId(ctx)
		return &Error{
			Code:       ErrorCodeNotFound,
			Message:    getMsg(object, status),
			TraceID:    traceId,
			HTTPStatus: http.StatusNotFound,
		}
	}

	ErrBadRequest = func(ctx context.Context) *Error {
		traceId := GetTraceId(ctx)
		return &Error{
			Code:       ErrorCodeBadRequest,
			Message:    DefaultBadRequestMessage,
			TraceID:    traceId,
			HTTPStatus: http.StatusBadRequest,
		}
	}

	// Status 5xx *******

	ErrSystemError = func(ctx context.Context, detail string) *Error {
		traceId := GetTraceId(ctx)
		return &Error{
			Code:       ErrorCodeSystemError,
			Message:    DefaultServerErrorMessage,
			TraceID:    traceId,
			HTTPStatus: http.StatusInternalServerError,
			Detail:     detail,
		}
	}

	ErrForbidden = func(ctx context.Context) *Error {
		traceId := GetTraceId(ctx)
		return &Error{
			Code:       ErrorCodeForbidden,
			Message:    DefaultForbiddenMessage,
			TraceID:    traceId,
			HTTPStatus: http.StatusForbidden,
		}
	}
)

func getMsg(object, status string) string {
	return fmt.Sprintf("%s %s", object, status)
}

const (
	DefaultServerErrorMessage  = "Something has gone wrong, please contact admin"
	DefaultBadRequestMessage   = "Invalid request"
	DefaultUnauthorizedMessage = "Token invalid"
	DefaultForbiddenMessage    = "Forbidden"
)
