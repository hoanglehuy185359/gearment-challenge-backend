package domain

import (
	"context"
	"gearment-backend-challenge/src/common"
	"gearment-backend-challenge/src/present/requests"
	"time"
)

type Book struct {
	Id            int64     `json:"id"`
	Title         string    `json:"title"`
	Author        string    `json:"author"`
	PublishedDate string    `json:"published_date"`
	Isbn          string    `json:"isbn"`
	NumberOfPages int64     `json:"number_of_pages"`
	CoverImage    string    `json:"cover_image"`
	Language      string    `json:"language"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}

func (b *Book) SetId(id int64) *Book {
	b.Id = id
	return b
}
func (b *Book) SetCreatedAt(createdAt time.Time) *Book {
	b.CreatedAt = createdAt
	return b
}

func NewBook(req *requests.BookReq) *Book {
	return &Book{
		Id:            0,
		Title:         req.Title,
		Author:        req.Author,
		PublishedDate: req.PublishedDate,
		Isbn:          req.Isbn,
		NumberOfPages: req.NumberOfPages,
		CoverImage:    req.CoverImage,
		Language:      req.Language,
		CreatedAt:     time.Now(),
		UpdatedAt:     time.Now(),
	}
}

type BookRepo interface {
	Save(ctx context.Context, book *Book) (*Book, *common.Error)
	GetById(ctx context.Context, id int64) (*Book, *common.Error)
	ListAll(ctx context.Context) ([]*Book, *common.Error)
	Delete(ctx context.Context, id int64) *common.Error
}

func (b *Book) TableName() string {
	return "book"
}
