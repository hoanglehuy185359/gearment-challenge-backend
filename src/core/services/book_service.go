package services

import (
	"context"
	"gearment-backend-challenge/src/common"
	"gearment-backend-challenge/src/common/helpers"
	"gearment-backend-challenge/src/common/log"
	"gearment-backend-challenge/src/core/domain"
	"gearment-backend-challenge/src/present/requests"
)

type BookService struct {
	bookRepo domain.BookRepo
}

func NewBookService(bookRepo domain.BookRepo) *BookService {
	return &BookService{
		bookRepo: bookRepo,
	}
}

func (s *BookService) Save(ctx context.Context, req *requests.BookReq) (*domain.Book, *common.Error) {
	book, err := s.bookRepo.Save(ctx, domain.NewBook(req))
	if err != nil {
		log.IErr(ctx, err)
		return nil, err
	}
	return book, nil
}

func (s *BookService) Get(ctx context.Context, id int64) (*domain.Book, *common.Error) {
	book, err := s.bookRepo.GetById(ctx, id)
	if err != nil {
		log.IErr(ctx, err)
		return nil, err
	}
	return book, nil
}

func (s *BookService) ListAll(ctx context.Context) ([]*domain.Book, *common.Error) {
	books, err := s.bookRepo.ListAll(ctx)
	if err != nil {
		log.IErr(ctx, err)
		return nil, err
	}
	return books, nil
}

func (s *BookService) Update(ctx context.Context, req *requests.UpdateBookReq) (*domain.Book, *common.Error) {
	bookExist, err := s.bookRepo.GetById(ctx, req.Id)
	if helpers.IsInternalError(err) {
		log.Error(ctx, err.Error())
		return nil, err
	}
	if err != nil {
		return nil, common.ErrNotFound(ctx, "book", "not found")
	}
	newBook := domain.NewBook(req.BookReq).
		SetId(bookExist.Id).
		SetCreatedAt(bookExist.CreatedAt)

	book, err := s.bookRepo.Save(ctx, newBook)
	if err != nil {
		log.IErr(ctx, err)
		return nil, err
	}
	return book, nil
}

func (s *BookService) Delete(ctx context.Context, id int64) *common.Error {
	bookExist, err := s.bookRepo.GetById(ctx, id)
	if helpers.IsInternalError(err) {
		log.Error(ctx, err.Error())
		return err
	}
	if bookExist == nil {
		return common.ErrNotFound(ctx, "book", "not found")
	}
	err = s.bookRepo.Delete(ctx, id)
	if err != nil {
		log.IErr(ctx, err)
	}
	return err
}
