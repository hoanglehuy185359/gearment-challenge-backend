package repo

import (
	"context"
	"errors"
	"gearment-backend-challenge/src/common"
	"gearment-backend-challenge/src/core/domain"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func NewBookRepo(base *baseRepo) domain.BookRepo {
	return &bookRepo{
		base,
	}
}

type bookRepo struct {
	*baseRepo
}

func (r bookRepo) GetById(ctx context.Context, id int64) (*domain.Book, *common.Error) {
	book := &domain.Book{}
	cond := clause.Eq{Column: "id", Value: id}
	if err := r.db.WithContext(ctx).Clauses(cond).Take(book).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, common.ErrNotFound(ctx, "book", "not found")
		}
		return nil, r.returnError(ctx, err)
	}
	return book, nil
}

func (r bookRepo) Save(ctx context.Context, book *domain.Book) (*domain.Book, *common.Error) {
	if err := r.db.WithContext(ctx).Save(&book).Error; err != nil {
		return nil, r.returnError(ctx, err)
	}
	return book, nil
}

func (r bookRepo) ListAll(ctx context.Context) ([]*domain.Book, *common.Error) {
	books := make([]*domain.Book, 0)
	if err := r.db.WithContext(ctx).Find(&books).Error; err != nil {
		return nil, r.returnError(ctx, err)
	}
	return books, nil
}

func (r bookRepo) Delete(ctx context.Context, id int64) *common.Error {
	book := &domain.Book{}
	cond := clause.Eq{Column: "id", Value: id}
	if err := r.db.WithContext(ctx).Clauses(cond).Delete(book).Error; err != nil {
		return r.returnError(ctx, err)
	}
	return nil
}
