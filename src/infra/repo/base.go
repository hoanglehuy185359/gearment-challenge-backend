package repo

import (
	"context"
	"gearment-backend-challenge/src/common"
	"gorm.io/gorm"
)

type baseRepo struct {
	db *gorm.DB
}

func NewBaseRepo(db *gorm.DB) *baseRepo {
	return &baseRepo{
		db: db,
	}
}

func (b *baseRepo) returnError(ctx context.Context, err error) *common.Error {
	return common.ErrSystemError(ctx, err.Error())
}
