package bootstrap

import (
	"context"
	"errors"
	"gearment-backend-challenge/src/common/configs"
	"gearment-backend-challenge/src/present/router"
	"github.com/gin-gonic/gin"
	"go.uber.org/fx"
	"go.uber.org/zap"
	"net/http"
)

func BuildHTTPServerModule() fx.Option {
	return fx.Options(
		fx.Provide(gin.New),
		fx.Invoke(router.RegisterHandler),
		fx.Invoke(router.RegisterGinRouters),
		fx.Invoke(NewHttpServer),
	)
}

func NewHttpServer(logger *zap.SugaredLogger, lc fx.Lifecycle, engine *gin.Engine) {
	server := &http.Server{
		Addr:    configs.Get().Server.Http.Address,
		Handler: engine,
	}
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			go func() {
				if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
					logger.Fatalf("Cannot start application due by error [%v]", err)
				}
			}()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			if err := server.Shutdown(ctx); err != nil {
				logger.Fatalf("Server forced to shutdown: [%v]", err)
			}
			logger.Infof("Stopping HTTP server")
			return nil
		},
	})
}
