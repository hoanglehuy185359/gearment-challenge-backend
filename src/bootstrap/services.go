package bootstrap

import (
	"gearment-backend-challenge/src/core/services"
	"go.uber.org/fx"
)

func BuildServiceModule() fx.Option {
	return fx.Options(
		fx.Provide(services.NewBookService),
	)
}
