package bootstrap

import (
	"gearment-backend-challenge/src/present/controllers"
	"gearment-backend-challenge/src/present/validator"
	"go.uber.org/fx"
)

func BuildControllerModule() fx.Option {
	return fx.Options(
		fx.Provide(controllers.NewBaseController),
		fx.Provide(controllers.NewBookController),
	)
}

func BuildValidator() fx.Option {
	return fx.Options(
		fx.Provide(validator.NewValidator),
		fx.Invoke(validator.RegisterValidations),
	)
}
