package bootstrap

import (
	"context"
	"fmt"
	"gearment-backend-challenge/src/common/configs"
	"gearment-backend-challenge/src/core/constant"
	"gearment-backend-challenge/src/core/domain"
	"gearment-backend-challenge/src/infra/repo"
	"github.com/uptrace/opentelemetry-go-extra/otelgorm"
	"go.uber.org/fx"
	"go.uber.org/zap"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func BuildStorageModules() fx.Option {
	return fx.Options(
		fx.Provide(newPostgresqlDB),

		fx.Provide(repo.NewBaseRepo),
		fx.Provide(repo.NewBookRepo),
	)
}

func newPostgresqlDB(lc fx.Lifecycle, log *zap.SugaredLogger) *gorm.DB {
	cf := configs.Get().Postgresql
	dsn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s password=%s", cf.Host,
		cf.Port, cf.User, cf.DbName, cf.SslMode, cf.Password)
	logMode := logger.Info
	if constant.IsProdEnv() {
		logMode = logger.Silent
	}
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logMode),
	})
	if err != nil {
		panic(err)
	}
	if cf.AutoMigrate {
		_ = db.AutoMigrate(domain.Book{})
	}
	if configs.Get().Tracer.Enabled {
		if err := db.Use(otelgorm.NewPlugin()); err != nil {
			panic(err)
		}
	}
	lc.Append(fx.Hook{
		OnStop: func(ctx context.Context) error {
			log.Debug("Coming OnStop Storage")
			sqlDB, err := db.DB()
			if err != nil {
				return err
			}
			return sqlDB.Close()
		},
	})
	return db
}
