package validator

import (
	"gearment-backend-challenge/src/common/log"
	"github.com/go-playground/validator/v10"
	"regexp"
)

const (
	timePattern = `^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])$`
)

func NewValidator() *validator.Validate {
	return validator.New()
}

func registerValidation(validator *validator.Validate, tag string, fn validator.Func) {
	if err := validator.RegisterValidation(tag, fn); err != nil {
		log.GetLogger().GetZap().Fatalf("Register custom validation %s failed with error: %s", tag, err.Error())
	}
	return
}

func RegisterValidations(validator *validator.Validate) {
	registerValidation(validator, "time-format", validDecimalRange)
}

var validDecimalRange validator.Func = func(fl validator.FieldLevel) bool {
	value, ok := fl.Field().Interface().(string)
	if !ok {
		return false
	}
	match, err := regexp.MatchString(timePattern, value)
	if err != nil {
		log.GetLogger().GetZap().Errorf("time-format doesn't match with regex: [%s]", err.Error())
	}
	return match
}
