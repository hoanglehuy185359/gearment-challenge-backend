package router

import (
	"gearment-backend-challenge/src/common/configs"
	"gearment-backend-challenge/src/present/controllers"
	"gearment-backend-challenge/src/present/middlewares"
	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"go.uber.org/fx"
)

type RoutersIn struct {
	fx.In
	Engine         *gin.Engine
	BookController *controllers.BookController
}

func RegisterHandler(engine *gin.Engine) {
	engine.Use(middlewares.Recovery())
	engine.Use(otelgin.Middleware(configs.Get().Server.Name))
	engine.Use(middlewares.Tracer())
	engine.Use(middlewares.Log())
}

func RegisterGinRouters(in RoutersIn) {
	in.Engine.Use(cors.AllowAll())

	group := in.Engine.Group(configs.Get().Server.Http.Prefix)
	group.GET("/ping", middlewares.HealthCheckEndpoint)
	// http swagger serve
	if configs.Get().Swagger.Enabled {
		group.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}
	registerPublicRouters(group, in)
}

func registerPublicRouters(r *gin.RouterGroup, in RoutersIn) {
	bookGroup := r.Group("/books")
	{
		bookGroup.POST("", in.BookController.Save)
		bookGroup.GET("/:id", in.BookController.Get)
		bookGroup.GET("", in.BookController.ListAll)
		bookGroup.PUT("/:id", in.BookController.Update)
		bookGroup.DELETE("/:id", in.BookController.Delete)

	}
}
