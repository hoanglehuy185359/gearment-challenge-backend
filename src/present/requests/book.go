package requests

type BookReq struct {
	Title         string `json:"title" validate:"required"`
	Author        string `json:"author" validate:"required"`
	PublishedDate string `json:"published_date" validate:"required,time-format"`
	Isbn          string `json:"isbn" validate:"required"`
	NumberOfPages int64  `json:"number_of_pages" validate:"required"`
	CoverImage    string `json:"cover_image" validate:"required"`
	Language      string `json:"language" validate:"required"`
}

type UpdateBookReq struct {
	Id int64 `uri:"id" validate:"required"`
	*BookReq
}
