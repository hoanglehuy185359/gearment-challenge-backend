package controllers

import (
	"gearment-backend-challenge/src/common"
	"gearment-backend-challenge/src/common/log"
	"gearment-backend-challenge/src/core/services"
	"gearment-backend-challenge/src/present/requests"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type BookController struct {
	*baseController
	service *services.BookService
}

func NewBookController(
	baseController *baseController,
	service *services.BookService,
) *BookController {
	return &BookController{
		baseController: baseController,
		service:        service,
	}
}

func (i *BookController) Save(c *gin.Context) {
	req := new(requests.BookReq)
	if err := i.BindAndValidateRequest(c, req); err != nil {
		i.ErrorData(c, err)
		return
	}
	book, ierr := i.service.Save(c.Request.Context(), req)
	if ierr != nil {
		i.ErrorData(c, ierr)
		return
	}
	i.Success(c, http.StatusCreated, book)
}

func (i *BookController) Get(c *gin.Context) {
	idParam := c.Param("id")
	ctx := c.Request.Context()
	id, err := strconv.Atoi(idParam)
	if err != nil {
		log.Error(ctx, err.Error())
		i.ErrorData(c, common.ErrBadRequest(ctx))
	}
	book, ierr := i.service.Get(ctx, int64(id))
	if ierr != nil {
		i.ErrorData(c, ierr)
		return
	}
	i.Success(c, http.StatusOK, book)
}

func (i *BookController) ListAll(c *gin.Context) {
	books, ierr := i.service.ListAll(c.Request.Context())
	if ierr != nil {
		i.ErrorData(c, ierr)
		return
	}
	i.Success(c, http.StatusOK, books)
}

func (i *BookController) Update(c *gin.Context) {
	req := new(requests.UpdateBookReq)
	if err := i.BindAndValidateRequest(c, req); err != nil {
		i.ErrorData(c, err)
		return
	}
	book, ierr := i.service.Update(c.Request.Context(), req)
	if ierr != nil {
		i.ErrorData(c, ierr)
		return
	}
	i.Success(c, http.StatusOK, book)
}

func (i *BookController) Delete(c *gin.Context) {
	idParam := c.Param("id")
	ctx := c.Request.Context()
	id, err := strconv.Atoi(idParam)
	if err != nil {
		log.Error(ctx, err.Error())
		i.ErrorData(c, common.ErrBadRequest(ctx))
	}
	ierr := i.service.Delete(ctx, int64(id))
	if ierr != nil {
		i.ErrorData(c, ierr)
		return
	}
	i.Success(c, http.StatusNoContent, nil)
}
