package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"gearment-backend-challenge/src/bootstrap"
	"gearment-backend-challenge/src/common"
	"gearment-backend-challenge/src/common/configs"
	"gearment-backend-challenge/src/common/log"
	"gearment-backend-challenge/src/core/constant"
	"go.uber.org/fx"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	defaultGracefulTimeout = 15 * time.Second
)

func init() {
	var pathConfig string
	flag.StringVar(&pathConfig, "config", "configs/config.yaml", "path to config file")
	flag.Parse()
	err := configs.LoadConfig(pathConfig)
	if err != nil {
		panic(err)
	}
	config, err := json.Marshal(configs.Get())
	if err != nil {
		panic(err)
	}
	if !constant.IsProdEnv() {
		fmt.Println(string(config))
	}
	log.NewLogger()
}

func main() {
	logger := log.GetLogger().GetZap()
	logger.Debugf("App %s is running", configs.Get().Mode)

	app := fx.New(
		fx.Provide(log.GetLogger().GetZap),
		fx.Invoke(common.InitTracer),

		bootstrap.BuildStorageModules(),
		//build service
		bootstrap.BuildServiceModule(),

		//build http server
		bootstrap.BuildControllerModule(),
		bootstrap.BuildValidator(),
		bootstrap.BuildHTTPServerModule(),
	)

	startCtx, cancel := context.WithTimeout(context.Background(), defaultGracefulTimeout)
	defer cancel()
	if err := app.Start(startCtx); err != nil {
		logger.Fatalf(err.Error())
	}

	interruptHandle(app, logger)
}

func interruptHandle(app *fx.App, logger *zap.SugaredLogger) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	logger.Debugf("Listening Signal...")
	s := <-c
	logger.Infof("Received signal: %s. Shutting down Server ...", s)

	stopCtx, cancel := context.WithTimeout(context.Background(), defaultGracefulTimeout)
	defer cancel()

	if err := app.Stop(stopCtx); err != nil {
		logger.Fatalf(err.Error())
	}
}
