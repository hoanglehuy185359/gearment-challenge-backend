# Gearment-challenge-backend

## Description
1. This project is for the Gearment Backend Software Engineer Coding Challenge.
2. The project is developed using Go 1.22 and PostgreSQL as the database. It also uses OpenTelemetry Jaeger for distributed tracing.

## Conditions
Before running the application, please ensure the following conditions are met:

1. Docker Compose is installed on your machine.
2. Ensure that no other applications are running on ports 8080, 5432, and 16686.

## How to Run Application
1. Clone the repository:
    ```bash
    git clone https://gitlab.com/hoanglehuy185359/gearment-challenge-backend.git
    ```

2. Navigate to the project directory:
    ```bash
    cd gearment-challenge-backend
    ```

3. Build and run the Docker containers:
    ```bash
    docker compose -f docker-compose.yml up
    ```

The application should now be running at `localhost:8080`.
Remember to prefix your API calls with `/api`. For example, if you have an endpoint named `endpoint`, you would access it at `localhost:8080/api/endpoint`.

You can access the Jaeger UI for tracing at `localhost:16686/search`.

To access the database, use the following credentials:
- Host: `localhost`
- Port: `5432`
- Database: `gearment`
- User: `root`
- Password: `1`

## Testing

To run the tests for the application, use the following command:

```bash
docker compose -f docker-compose-test.yml up

