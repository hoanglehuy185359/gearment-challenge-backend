package tests

import (
	"github.com/imroc/req/v3"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestGet200(t *testing.T) {
	id := getIdAvailable(t)
	client := req.C()
	resp, err := client.R().
		Get(baseUrl + "/" + id)
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusOK)

}

func TestGet404(t *testing.T) {
	client := req.C()
	resp, err := client.R().
		Get(baseUrl + "/1000")
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusNotFound)
}
