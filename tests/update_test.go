package tests

import (
	"github.com/imroc/req/v3"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestUpdate200(t *testing.T) {
	id := getIdAvailable(t)
	client := req.C()
	resp, err := client.R().
		SetBody(book).
		Put(baseUrl + "/" + id)
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusOK)

}

func TestUpdate400(t *testing.T) {
	client := req.C()
	resp, err := client.R().
		SetBody(invalidBook).
		Put(baseUrl + "/1")
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusBadRequest)
}

func TestUpdate404(t *testing.T) {
	client := req.C()
	resp, err := client.R().
		SetBody(book).
		Put(baseUrl + "/1000")
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusNotFound)
}
