package tests

import (
	"github.com/imroc/req/v3"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestList200(t *testing.T) {
	TestInsert200(t)
	client := req.C()
	resp, err := client.R().
		Get(baseUrl)
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusOK)
}
