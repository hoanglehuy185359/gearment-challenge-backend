package tests

import (
	"github.com/imroc/req/v3"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

type Response struct {
	Books []*Book `json:"books"`
}

func TestDelete200(t *testing.T) {
	id := getIdAvailable(t)
	client := req.C()
	resp, err := client.R().
		Delete(baseUrl + "/" + id)
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusNoContent)
}

func TestDelete404(t *testing.T) {
	client := req.C()
	resp, err := client.R().
		Delete(baseUrl + "/1000")
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusNotFound)
}
