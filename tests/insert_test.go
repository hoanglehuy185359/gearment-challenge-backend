package tests

import (
	"github.com/imroc/req/v3"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestInsert200(t *testing.T) {
	client := req.C()
	resp, err := client.R().
		SetBody(book).
		Post(baseUrl)
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusCreated)
}

func TestInsert400(t *testing.T) {
	client := req.C()
	resp, err := client.R().
		SetBody(invalidBook).
		Post(baseUrl)
	failure := assert.Nil(t, err)
	if failure != true {
		return
	}
	assert.Equal(t, resp.StatusCode, http.StatusBadRequest)
}
