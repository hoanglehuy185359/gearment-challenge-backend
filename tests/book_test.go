package tests

import (
	"github.com/imroc/req/v3"
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

type BookReq struct {
	Title         string `json:"title"`
	Author        string `json:"author"`
	PublishedDate string `json:"published_date"`
	Isbn          string `json:"isbn"`
	NumberOfPages int64  `json:"number_of_pages"`
	CoverImage    string `json:"cover_image"`
	Language      string `json:"language"`
}

var book = &BookReq{
	Title:         "The Da Vinci Code",
	Author:        "2",
	PublishedDate: "2002-10-20",
	Isbn:          "3",
	NumberOfPages: 100,
	CoverImage:    "4",
	Language:      "5",
}

var invalidBook = &BookReq{
	Title:         "The Da Vinci Code",
	Author:        "2",
	PublishedDate: "2002-20-20",
	Isbn:          "3",
	NumberOfPages: 100,
	CoverImage:    "4",
	Language:      "5",
}

type Book struct {
	Id int64 `json:"id"`
	*BookReq
}

const baseUrl = "http://localhost:8080/api/books"

func getIdAvailable(t *testing.T) string {
	TestInsert200(t)
	var response Response
	client := req.C()
	_, err := client.R().
		SetSuccessResult(&response.Books).
		Get(baseUrl)

	failure := assert.Nil(t, err)
	if failure != true {
		return ""
	}
	books := response.Books
	failure = assert.NotEqual(t, len(books), 0)
	if failure != true {
		return ""
	}
	id := books[0].Id
	return strconv.FormatInt(id, 10)
}
