#Build
FROM golang:1.22-alpine AS builder

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .
ENV CGO_ENABLED=0
ARG GOARCH_ARG=amd64

RUN go build -o gearment

# Runtime
FROM alpine:3.5
WORKDIR /app

COPY --from=builder /app/gearment ./app-gearment

ENTRYPOINT ["/app/app-gearment"]